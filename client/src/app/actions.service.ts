import { EventEmitter, Injectable } from '@angular/core';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { PLANET } from './landing-page/landing-page.component';
import { PlanetModalComponent } from './planet-modal/planet-modal.component';
import { isObject } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ActionsService {
  planetsData: EventEmitter<PLANET[]> = new EventEmitter();
  planetDataChanged: EventEmitter<PLANET> = new EventEmitter();

  constructor(private http: HttpClient, public dialog: MatDialog, private router: Router) {
  }

  createPlanet = () => {
    this.openModal(null, 'landingPage');
  }

  deletePlanet = (id: string, from: string) => {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);

    dialogRef.afterClosed().subscribe((data: any) => {
      if (data === 'confirm') {
        this.http.delete('/api/planets/' + id).subscribe((response: any) => {
          if (from !== 'landingPage') {
            this.router.navigate(['/landingPage']);
          } else {
            this.getPlanetsData();
          }
        });
      }
    });
  }

  editPlanet = (planet: PLANET, from) => {
    this.openModal(planet, from);
  }

  getPlanetsData = () => {
    this.http.get('/api/planets').subscribe((data: PLANET[]) => {
      this.planetsData.emit(data);
    });
  }

  getPlanetData = (id: number) => {
    this.http.get('/api/planets/' + id).subscribe((data: PLANET) => {
      this.planetDataChanged.emit(data);
    });
  }

  mapDataForSave = (data: any) => {
    const mappedData = {
      planetName: data.planetName,
      description: data.planetDescription,
      distInMillionsKM: {
        fromEarth: data.distanceFromEarth,
        fromSun: data.distanceFromSun
      },
      planetRadiusKM: data.planetRadiusKM,
      planetColor: data.planetColor
    };
    if (data.id) {
      Object.assign(mappedData, { id: data.id });
    }
    return mappedData;
  }

  private openModal = (planet: PLANET | null, from: string) => {
    const dialogRef = this.dialog.open(PlanetModalComponent, {
      panelClass: 'planet-modal',
      width: '450px',
      data: planet
    });
    dialogRef.afterClosed().subscribe((data: any) => {
      if (data) {
        const formData = new FormData();
        if (typeof data.planetImage !== 'string') {
          formData.append('file', data.planetImage);
        } else {
          formData.append('imageUrl', data.planetImage);
        }
        // Might be optimized by matching reactive form data with props that are sending to SS.
        // That way we would not need mapDataForSave method
        const dataForSave = this.mapDataForSave(data);
        for (const key in dataForSave) {
          if (dataForSave.hasOwnProperty(key)) {
            if (isObject(dataForSave[key])) {
              Object.keys(dataForSave[key]).forEach((currentKey: any) => {
                formData.append(`${key}[${currentKey}]`, dataForSave[key][currentKey]);
              });
            } else {
              formData.append(key, dataForSave[key]);
            }
          }
        }

        let url = '/api/planets/';
        let method = 'post';
        if (planet?.id) {
          url += planet.id;
          method = 'put';
        }
        this.http[method](url, formData).subscribe(
          (res) => {
            if (from === 'singlePlanet') {
              this.getPlanetData(planet?.id);
            } else {
              this.getPlanetsData();
            }
          },
          (err) => console.log(err)
        );
      }
    });
  }
}

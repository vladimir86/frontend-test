import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { SinglePlanetComponent } from './single-planet/single-planet.component';


const routes: Routes = [{
  path: '',
  redirectTo: '/landingPage',
  pathMatch: 'full'
}, {
  path: 'landingPage',
  component: LandingPageComponent
}, {
  path: 'landingPage/:id',
  component: SinglePlanetComponent
}, {
  path: '**',
  redirectTo: '/landingPage'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

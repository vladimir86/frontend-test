import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { SinglePlanetComponent } from './single-planet/single-planet.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { PlanetModalComponent } from './planet-modal/planet-modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { MatSortModule } from '@angular/material/sort';
import { FilterPipe } from './filter.pipe';
import { ActionsService } from './actions.service';
import { GridViewComponent } from './landing-page/grid-view/grid-view.component';
import { TableViewComponent } from './landing-page/table-view/table-view.component';

@NgModule({
  declarations: [AppComponent, LandingPageComponent, SinglePlanetComponent, PlanetModalComponent, ConfirmationDialogComponent, FilterPipe, GridViewComponent, TableViewComponent],
  imports: [
    MatDialogModule,
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonToggleModule,
    MatTableModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,
    MatSortModule
  ],
  exports: [FilterPipe],
  providers: [ActionsService],
  bootstrap: [AppComponent],
})
export class AppModule {}

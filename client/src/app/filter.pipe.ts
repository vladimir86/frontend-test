import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  /**
   * Filter items depending on args. First arg is search string, second is column name
   */
  transform(items: any[], args: string[]): any[] {
    if (!items) {
      return [];
    }
    if (!args[0]) {
      return items;
    }

    return items.filter(item => {
      return String(item[args[1]]).toLowerCase().includes(args[0].toLowerCase());
    });
  }

}

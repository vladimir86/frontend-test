import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActionsService } from '../actions.service';

export type PLANET = {
  planetName: string,
  id: number,
  description: string,
  imageUrl: string,
  imageName: string,
  planetColor: string,
  distInMillionsKM: {
    fromEarth: number,
    fromSun: number
  },
  planetRadiusKM: number
};

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  planets: PLANET[];
  tableType = 'table';

  constructor(private http: HttpClient, private actionService: ActionsService) {
  }

  ngOnInit(): void {
    this.actionService.getPlanetsData();
    this.actionService.planetsData.subscribe((data: PLANET[]) => this.planets = data);
  }

  toggleTable = (event: any) => {
    this.tableType = event.value;
  };

  createNewPlanet = () => {
    this.actionService.createPlanet();
  };

}

import { Component, Input, OnInit } from '@angular/core';
import { PLANET } from '../landing-page.component';
import { Sort } from '@angular/material/sort';
import { includes } from 'lodash';
import { ActionsService } from '../../actions.service';

@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.scss']
})
export class TableViewComponent {
  @Input() planets: PLANET[];
  searchText: string;

  constructor(private actionService: ActionsService) {
  }

  sortData = (sort: Sort) => {
    const data = this.planets.slice();
    if (!sort.active || sort.direction === '') {
      this.planets = data;
      return;
    }

    this.planets = data.sort((a: any, b: any) => {
      const isAsc = sort.direction === 'asc';
      let firstPar = a[sort.active];
      let secondPar = b[sort.active];
      if (includes(['fromEarth', 'fromSun'], sort.active)) {
        firstPar = a.distInMillionsKM[sort.active];
        secondPar = b.distInMillionsKM[sort.active];
      }
      return (firstPar < secondPar ? -1 : 1) * (isAsc ? 1 : -1);
    });
  };

}

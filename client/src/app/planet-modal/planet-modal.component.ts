import { AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PLANET } from '../landing-page/landing-page.component';

@Component({
  selector: 'app-planet-modal',
  templateUrl: './planet-modal.component.html',
  styleUrls: ['./planet-modal.component.scss']
})
export class PlanetModalComponent implements OnInit, AfterViewInit {
  registerForm: FormGroup;
  @ViewChild('sourcePreview') sourcePreview: ElementRef;

  titlePlaceholder = 'Create new';

  constructor(
    public dialogRef: MatDialogRef<PlanetModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PLANET,
    private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      id: [this.data?.id || null],
      distanceFromSun: [this.data?.distInMillionsKM?.fromSun || null],
      distanceFromEarth: [this.data?.distInMillionsKM?.fromEarth || null],
      planetName: [this.data?.planetName || '', Validators.required],
      planetColor: [this.data?.planetColor || ''],
      planetDescription: [this.data?.description || '', [Validators.required, Validators.minLength(6)]],
      planetImage: [this.data?.imageUrl || '', Validators.required],
      planetRadiusKM: [this.data?.planetRadiusKM || null]
    });
    if (this.data?.id) {
      this.titlePlaceholder = 'Edit';
    }
  }

  ngAfterViewInit(): void {
    if (this.data?.imageUrl) {
      this.sourcePreview.nativeElement.setAttribute('src', this.data.imageUrl);
    }
  }

  // tslint:disable-next-line:typedef
  get form() {
    return this.registerForm.controls;
  }

  onReset = () => {
    this.registerForm.reset();
    this.dialogRef.close();
  };

  openFileList = (uploadField: any) => {
    uploadField.click();
  };

  uploadOnChange = ({ target }: any, sourcePreview) => {
    if (target.files && target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        sourcePreview.setAttribute('src', e.target.result);
      };

      reader.readAsDataURL(target.files[0]);
      this.registerForm.patchValue({ planetImage: target.files[0] }, { onlySelf: true });
    }
  }
}

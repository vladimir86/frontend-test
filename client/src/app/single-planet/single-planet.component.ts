import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ActionsService } from '../actions.service';
import { PLANET } from '../landing-page/landing-page.component';

@Component({
  selector: 'app-single-planet',
  templateUrl: './single-planet.component.html',
  styleUrls: ['./single-planet.component.scss']
})
export class SinglePlanetComponent implements OnInit {
  planet: any;
  routeSub: Subscription;
  id: number;

  constructor(private http: HttpClient,
              private route: ActivatedRoute,
              private actionService: ActionsService) {
  }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe((params: { id: number }) => {
      if (params.id) {
        this.id = params.id;
        this.actionService.getPlanetData(this.id);
      }
    });
    this.actionService.planetDataChanged.subscribe((data: PLANET) => this.planet = data);
  }

  displayTagCondition = (value: number | string) => {
    let returnValue = !!value;
    if (typeof value === 'string') {
      returnValue = !!parseFloat(value);
    }
    return returnValue;
  }
}
